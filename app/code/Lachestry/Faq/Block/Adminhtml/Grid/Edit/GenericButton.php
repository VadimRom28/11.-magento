<?php

namespace Lachestry\Faq\Block\Adminhtml\Grid\Edit;

use Magento\Backend\Block\Widget\Context;

class GenericButton
{
    protected $urlBuilder;

    public function __construct(Context $context)
    {
        $this->urlBuilder = $context->getUrlBuilder();
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
