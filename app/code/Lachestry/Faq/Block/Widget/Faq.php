<?php
namespace Lachestry\Faq\Block\Widget;

use Lachestry\Faq\Api\Data\FaqInterface;
use Lachestry\Faq\Model\Config;
use Lachestry\Faq\Model\FaqFactory;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Faq extends Template implements BlockInterface
{
    protected $_template = 'widget/faq.phtml';
    protected $collectionItems;
    protected $faqConfig;


    public function __construct(
        Template\Context $context,
        FaqFactory $faqFactory,
        Config $faqConfig,
        array $data = []
    ) {
        $this->faqConfig = $faqConfig;
        $this->collectionItems = $faqFactory->create()
            ->getCollection()
            ->setPageSize($this->getMaxCount())
            ->getItems();
        parent::__construct($context, $data);
    }


    public function getItems(): array
    {
        $items = [];

        foreach ($this->collectionItems as $item) {
            $itemData = $item->getData();

            if ($itemData[FaqInterface::IS_ENABLED]) {
                $items[] = $itemData;
            }
        }

        return $items;
    }

    public function canDisplay(): bool
    {
        $isProductPage = $this->getRequest()->getFullActionName() === 'catalog_product_view';
        $isDisplayAllowed = $this->faqConfig->getConfig('faq/view/view_on_product');

        return !$isProductPage || $isDisplayAllowed;
    }


    private function getMaxCount()
    {
        return $this->faqConfig->getConfig('faq/view/max_qty');
    }
}
