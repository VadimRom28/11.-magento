<?php

namespace Lachestry\Faq\Model;

use Lachestry\Faq\Api\Data\FaqInterface;
use Lachestry\Faq\Model\ResourceModel\Faq as FaqResource;
use Magento\Framework\Model\AbstractModel;

class Faq extends AbstractModel implements FaqInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    protected $_idFieldName = FaqInterface::ID;

    protected function _construct()
    {
        $this->_init(FaqResource::class);
    }

    public function getQuestion(): string
    {
        return $this->getData(FaqInterface::QUESTION);
    }

    public function setQuestion(string $question): FaqInterface
    {
        $this->setData(FaqInterface::QUESTION, $question);
        return $this;
    }

    public function getAnswer(): string
    {
        return $this->getData(FaqInterface::ANSWER);
    }

    public function setAnswer(string $answer): FaqInterface
    {
        $this->setData(FaqInterface::ANSWER, $answer);
        return $this;
    }

    public function isEnabled(): ?bool
    {
        return (bool)$this->getData(FaqInterface::IS_ENABLED);
    }

    public function setIsEnabled(?bool $isEnabled): FaqInterface
    {
        return $this->setData(self::IS_ENABLED, $isEnabled);
    }

    public function getCreatedAt(): ?string
    {
        return $this->getData(FaqInterface::CREATED_AT);
    }

    public function setCreatedAt(string $createdAt): FaqInterface
    {
        $this->setData(FaqInterface::CREATED_AT, $createdAt);
        return $this;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->getData(FaqInterface::UPDATED_AT);
    }

    public function setUpdatedAt(string $updatedAt): FaqInterface
    {
        $this->setData(FaqInterface::UPDATED_AT, $updatedAt);
        return $this;
    }
}
