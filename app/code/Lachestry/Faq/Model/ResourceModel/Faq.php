<?php

namespace Lachestry\Faq\Model\ResourceModel;

use Lachestry\Faq\Api\Data\FaqInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Faq extends AbstractDb
{
    const TABLE_NAME = 'lachestry_faq';

    protected function _construct(): void
    {
        $this->_init(self::TABLE_NAME, FaqInterface::ID);
    }
}
