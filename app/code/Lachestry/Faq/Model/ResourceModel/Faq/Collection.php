<?php

namespace Lachestry\Faq\Model\ResourceModel\Faq;

use Lachestry\Faq\Model\Faq;
use Lachestry\Faq\Model\ResourceModel\Faq as FaqResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct(): void
    {
        $this->_init(Faq::class, FaqResource::class);
    }
}
