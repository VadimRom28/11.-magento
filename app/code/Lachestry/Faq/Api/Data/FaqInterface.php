<?php

namespace Lachestry\Faq\Api\Data;

interface FaqInterface
{
    const ID         = 'id';
    const QUESTION   = 'question';
    const ANSWER     = 'answer';
    const IS_ENABLED = 'is_enabled';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getQuestion(): string;

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion(string $question): self;

    /**
     * @return string
     */
    public function getAnswer(): string;

    /**
     * @param string $answer
     * @return $this
     */
    public function setAnswer(string $answer): self;

    /**
     * @return bool|null
     */
    public function isEnabled(): ?bool;

    /**
     * @param bool|null $isEnabled
     * @return $this
     */
    public function setIsEnabled(?bool $isEnabled): self;

    /**
     * @return string
     */
    public function getCreatedAt(): ?string;

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt): self;

    /**
     * @return string
     */
    public function getUpdatedAt(): ?string;

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt): self;
}
