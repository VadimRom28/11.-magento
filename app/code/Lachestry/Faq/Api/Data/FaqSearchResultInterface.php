<?php

namespace Lachestry\Faq\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface FaqSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Lachestry\Faq\Api\Data\FaqInterface[]
     */
    public function getItems(): array;

    /**
     * @param \Lachestry\Faq\Api\Data\FaqInterface[] $items
     * @return $this
     */
    public function setItems(array $items): self;
}
