<?php

namespace Lachestry\Faq\Api;

use Lachestry\Faq\Api\Data\FaqInterface;
use Lachestry\Faq\Api\Data\FaqSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface FaqRepositoryInterface
{
    /**
     * @param int $id
     * @return FaqInterface
     */
    public function get(int $id): FaqInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return FaqSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param FaqInterface $faq
     * @return FaqInterface
     */
    public function save(FaqInterface $faq): FaqInterface;

    /**
     * @param FaqInterface $faq
     * @return bool
     */
    public function delete(FaqInterface $faq): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}

