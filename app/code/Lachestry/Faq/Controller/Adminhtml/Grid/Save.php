<?php

namespace Lachestry\Faq\Controller\Adminhtml\Grid;

use Lachestry\Faq\Api\FaqRepositoryInterface;
use Lachestry\Faq\Model\Faq;
use Lachestry\Faq\Model\FaqFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Lachestry_Faq::save';

    private $faqFactory;
    private $faqRepository;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        FaqFactory $faqFactory,
        FaqRepositoryInterface $faqRepository
    ) {
        $this->faqFactory = $faqFactory;
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
        $this->resultRedirect = $this->resultRedirectFactory->create();
    }


    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if (!$data) {
            return $this->resultRedirect->setPath('*/*/');
        }

        if (empty($data[Faq::ID])) {
            $data[Faq::ID] = null;
        }

        $faq = $this->faqFactory->create();

        $id = $this->getRequest()->getParam(Faq::ID);
        if ($id) {
            try {
                $faq = $this->faqRepository->get($id);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage(__('This FAQ no longer exists.'));
                return $this->resultRedirect->setPath('*/*/');
            }
        }

        $faq->setData($data);

        try {
            $faq->save();
            $this->messageManager->addSuccessMessage(__('You saved the FAQ.'));

            return $this->resultRedirect->setPath('*/*/');
        } catch (LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
        } catch (\Throwable $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('Something went wrong while saving the FAQ.')
            );
        }
    }
}
