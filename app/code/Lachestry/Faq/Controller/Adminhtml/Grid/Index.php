<?php

namespace Lachestry\Faq\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Lachestry_Faq::faq_admin_grid_index';
    const MENU_ITEM = 'Lachestry_Faq::faq_admin_grid_index';
    const TITLE = 'FAQ';


    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu(self::MENU_ITEM);
        $resultPage->getConfig()->getTitle()->prepend(__(self::TITLE));
        return $resultPage;
    }
}
