<?php

namespace Lachestry\Faq\Controller\Adminhtml\Grid;

use Lachestry\Faq\Api\FaqRepositoryInterface;
use Lachestry\Faq\Model\Faq;
use Lachestry\Faq\Model\FaqFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Lachestry_Faq::edit';
    const MENU_ITEM = 'Lachestry_Faq::faq_admin_grid';
    const TITLE = 'FAQ';

    protected $pageFactory;
    protected $faqFactory;
    protected $faqRepository;
    protected $resultRedirect;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        FaqFactory $faqFactory,
        FaqRepositoryInterface $faqRepository
    ) {
        $this->pageFactory = $pageFactory;
        $this->faqFactory = $faqFactory;
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
        $this->resultRedirect = $this->resultRedirectFactory->create();
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam(Faq::ID);

        $editedFaq = null;
        if ($id) {
            try {
                $editedFaq = $this->faqRepository->get($id);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('This FAQ no longer exists.'));
                return $this->resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit FAQ') : __('New FAQ'),
            $id ? __('Edit FAQ') : __('New FAQ')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('FAQ'));
        $resultPage->getConfig()->getTitle()
            ->prepend($editedFaq ? "Edit FAQ: {$editedFaq->getId()}" : __('New FAQ'));

        return $resultPage;
    }


    protected function _initAction()
    {
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu(self::MENU_ITEM);
        return $resultPage;
    }
}
