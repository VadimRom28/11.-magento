<?php

namespace Lachestry\Faq\Setup;

use Lachestry\Faq\Api\Data\FaqInterface;
use Lachestry\Faq\Model\ResourceModel\Faq;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package Toptal\Blog\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install Blog Posts table
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $tableName = Faq::TABLE_NAME;

        if ($setup->getConnection()->isTableExists($tableName)) {
            return;
        }

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable($tableName))
            ->addColumn(
                FaqInterface::ID,
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_IDENTITY  => true,
                    Table::OPTION_UNSIGNED  => true,
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_PRIMARY   => true
                ],
                'ID'
            )
            ->addColumn(
                FaqInterface::QUESTION,
                Table::TYPE_TEXT,
                null,
                [],
                'Question'
            )
            ->addColumn(
                FaqInterface::ANSWER,
                Table::TYPE_TEXT,
                '8k',
                [],
                'Answer'
            )
            ->addColumn(
                FaqInterface::IS_ENABLED,
                Table::TYPE_BOOLEAN,
                null,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_DEFAULT   => true
                ],
                'Enabled'
            )
            ->addColumn(
                FaqInterface::CREATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_DEFAULT   => Table::TIMESTAMP_INIT
                ],
                'Created At'
            )
            ->addColumn(
                FaqInterface::UPDATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_DEFAULT   => Table::TIMESTAMP_INIT_UPDATE
                ],
                'Updated At'
            )
            ->setComment('FAQ Table');

        $installer->getConnection()->createTable($table);

        $installer->getConnection()->addIndex(
            $installer->getTable($tableName),
            $setup->getIdxName(
                $installer->getTable($tableName),
                [
                    FaqInterface::QUESTION,
                    FaqInterface::ANSWER
                ],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            [
                FaqInterface::QUESTION,
                FaqInterface::ANSWER
            ],
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        $installer->endSetup();
    }
}
